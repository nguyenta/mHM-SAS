[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7228149.svg)](https://doi.org/10.5281/zenodo.7228149)

# mHM-SAS model

Catchment scale nitrate transport model based on the mHM-Nitrate model and StorAge Selection functions

# Version note

- The version that used for the paper "Nguyen, T. V., Kumar, R., Lutz, S. R., Musolff, A., Yang, J., & Fleckenstein, J. H. (2021). Modeling nitrate export from a mesoscale catchment using storage selection functions. Water Resources Research, 57, e2020WR028490. https://doi.org/10.1029/2020WR028490" was committed on 29 Jul, 2020 with the tag name v.1.0

- The version that used for the paper "Nguyen, T. V, Kumar, R., Musolff, A., Lutz, S. R., Sarrazin, F., Attinger, S., & Fleckenstein, J. (2021). Disparate Seasonal Nitrate Export from Nested Heterogeneous Subcatchments Revealed with StorAge Selection Functions. Earth and Space Science Open Archive ESSOAr. https://doi.org/10.1002/essoar.10507516.1" was committed on 9 Jul, 2021 with the tag name v.2.0
