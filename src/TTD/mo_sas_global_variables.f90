!> \file mo_sas_global_variables.f90

!> \brief Global variables used for water quality modelling based on TTDs.

!> \details
!> \note: time variables (e.g, max_age, deni) have a unit of timetep, e.g., 
!>        if the master equation is call at daily, then the unit is day
!>        ..................................monthly (30 days), then the unit is month (x 30 days)

!> \authors Tam Nguyen
!> \date June 2020


module mo_sas_global_variables

  
  use mo_kind,                                   only : i4, dp

  implicit none

  integer(i4),                              public    :: sas_function   ! selection function (1 = powerlaw, 2 = beta)
  integer(i4),                              public    :: max_age         ! maximum age in storage [timestep]
  real(dp),                                 public    :: max_old_fraction         ! maximum fraction of water in the oldest water pool [timestep]
  real(dp),                                 public    :: ini_storage

  type sas_para
     real(dp), dimension(:), allocatable              :: stor_age       ! Residence time distribution
     real(dp), dimension(:), allocatable              :: conc_age       ! Concentration distribution
     real(dp)                                         :: ka             ! k (or a) parameter of the powerlaw (or beta)
     real(dp)                                         :: b              ! beta parameter (b)
     real(dp)                                         :: half_life      ! half_life of nitrate [timestep] !change to ln(2)/half_life
  end type sas_para

  type(sas_para), dimension(3), public                :: L1_sas 

  character(256),                           public    :: dir_input_sas
  character(256),                           public    :: dir_output_sas 
  character(*), parameter                             :: file_sas = "sas_config.nml"

 !output variables
  type n_oput                                  
    real(dp)                                         :: N_input   !fertilizer, manure, residual, wet atmospheric deposition
    real(dp)                                         :: N_min     !mineralization
    real(dp)                                         :: N_uptake  !plant uptake
    real(dp)                                         :: N_deni    !denitrification
    real(dp)                                         :: N_leach   !leaching
  end type n_oput

  real(dp), dimension(3)                             :: pre_inflow
  real(dp), dimension(3)                             :: pre_outflow           
  real(dp), dimension(3)                             :: length
  real(dp), dimension(3)                             :: width
  real(dp), dimension(3)                             :: depth
  real(dp), dimension(3)                             :: bank_slope
  real(dp), dimension(3)                             :: chanel_slope
  real(dp), dimension(3)                             :: maning_n
  real(dp), dimension(3)                             :: rch_stor 

  real(dp)                                           :: x
  real(dp)                                           :: musk_coef_1
  real(dp)                                           :: musk_coef_2

  type(n_oput), dimension(:), allocatable            :: N_output

  type routing_para
    real(dp), dimension(3)                             :: pre_inflow
    real(dp), dimension(3)                             :: pre_outflow           
    real(dp), dimension(3)                             :: length
    real(dp), dimension(3)                             :: width
    real(dp), dimension(3)                             :: depth
    real(dp), dimension(3)                             :: bank_slope
    real(dp), dimension(3)                             :: chanel_slope
    real(dp), dimension(3)                             :: maning_n
    real(dp), dimension(3)                             :: rch_stor
    real(dp), dimension(3)                             :: stream_temp
    real(dp), dimension(3)                             :: inflow
    real(dp), dimension(3)                             :: cinflow
    real(dp), dimension(3)                             :: outflow
    real(dp), dimension(3)                             :: coutflow
    real(dp), dimension(3)                             :: crch_stor
    real(dp), dimension(3)                             :: q_yearly
    real(dp)                                           :: deni_rate
    real(dp)                                           :: x     
    real(dp)                                           :: musk_coef_1
    real(dp)                                           :: musk_coef_2
  end type routing_para

  type(routing_para)                                   :: L11_routing

  type warm_up_data
    real(dp), dimension(:,:), allocatable :: inflow
    real(dp), dimension(:,:), allocatable :: cinflow
    real(dp), dimension(:,:), allocatable :: outflow
  end type warm_up_data

  type(warm_up_data) :: L1_warm_up

  integer(i4), dimension(3)        :: start_warm_up
  integer(i4), dimension(3)        :: end_warm_up
  integer(i4)                      :: iter_warm_up

end module mo_sas_global_variables





