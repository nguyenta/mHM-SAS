!>       \file mo_flow_nutrient_routing.f90

!>       \brief TODO

!>       \details TODO

!>       \authors Tam Nguyen

!>       \date June 2020
!> Ref
!        weight_factor (x) = 0.1-0.3 MAIDMENT D. R., FREAD D.L., 1993. Handbook of Hydrology, Chap. 10, Flow Routing, – chap. 10, ISBN
!                                    0070397325 / 9780070397323, McGraw-Hill, 1424 pages

module mo_routing

  use mo_kind,                    only: i4, dp
  use mo_global_variables,        only: dirMorpho
  use mo_sas_global_variables,    only: routing_para
  

  implicit none

  public :: muskingum
  public :: read_routing_data
  public :: routing
  public :: initilize_routing
  public :: stream_temperature


contains

  !************************************************************************************************
  !Instream water and nutrient routing
  !************************************************************************************************

  subroutine routing(L11_routing)

    implicit none
    
    type(routing_para), intent(inout)  :: L11_routing

    real(dp)                           :: inflow, qtest(3), ctest(3) 
    integer(i4)                        :: i 

    do i = 3, 1, -1
      !water routing
      call muskingum(L11_routing%pre_inflow(i),     & 
                     L11_routing%inflow(i),         &
                     L11_routing%pre_outflow(i),    &
                     L11_routing%outflow(i),        &
                     L11_routing%x,                 &
                     L11_routing%musk_coef_1,       &
                     L11_routing%musk_coef_2,       &
                     L11_routing%length(i),         &
                     L11_routing%width(i),          &
                     L11_routing%depth(i),          &
                     L11_routing%bank_slope(i),     &
                     L11_routing%chanel_slope(i),   &
                     L11_routing%maning_n(i),       &
                     L11_routing%rch_stor(i)) 
      !instream processes
      call instream_deni(L11_routing%stream_temp(i),     & !degree
                         L11_routing%inflow(i),          & !m3/s
                         L11_routing%cinflow(i),         & !mg/L or g/m3
                         L11_routing%outflow(i),         & !m3/s
                         L11_routing%coutflow(i),        & !mg/L or g/m3
                         L11_routing%rch_stor(i),        & !m3
                         L11_routing%crch_stor(i),       & !mg/L or g/m3
                         L11_routing%deni_rate,          & ![1/day]
                         L11_routing%length(i),          & !
                         L11_routing%q_yearly(i))          !beneath area of reach [m2]

      !check if the current reach get inflow from other upstream reach
      if (i > 1) then
        !inflow to downstream reach
        inflow = L11_routing%inflow(i - 1) + L11_routing%outflow(i)
     
        !concentration in inflow to downstream reach
        if (inflow .gt. 0.0_dp) then
          L11_routing%cinflow(i - 1) = (L11_routing%cinflow(i - 1)* L11_routing%inflow(i - 1) + &
                                        L11_routing%coutflow(i) * L11_routing%outflow(i))/inflow 
        else
          L11_routing%cinflow(i - 1) = 0.0_dp
        end if

        !update inflow to the downstream reach
        L11_routing%inflow(i - 1) = inflow 

      end if
    end do

  end subroutine routing

  !************************************************************************************************
  !Subroutine initialized routing
  !************************************************************************************************
  subroutine initilize_routing(L11_routing)

    implicit none
    type(routing_para), intent(inout)      :: L11_routing

    L11_routing%pre_inflow(:)  = 1.0_dp
    L11_routing%cinflow(:)     = 1.0_dp
    L11_routing%rch_stor(:)    = 1.0_dp
    L11_routing%q_yearly(:)    = 1.0_dp
    L11_routing%stream_temp(:) = 1.0_dp

  end subroutine initilize_routing
  !************************************************************************************************
  !Subroutine instream processes
  !************************************************************************************************
  subroutine instream_deni(stream_temp,     & !degree
                           inflow,          & !m3/s
                           cinflow,         & !mg/L or g/m3
                           outflow,         & !m3/s
                           coutflow,        & !mg/L or g/m3
                           rch_stor,        & !m3
                           crch_stor,       & !mg/L or g/m3
                           deni_rate,       & ![1/day]
                           length,          & !
                           q_yearly)          !beneath area of reach [m2]

    implicit none

    real(dp), intent(in)               :: stream_temp
    real(dp), intent(in)               :: inflow
    real(dp), intent(in)               :: cinflow
    real(dp), intent(in)               :: outflow
    real(dp), intent(out)              :: coutflow
    real(dp), intent(in)               :: rch_stor 
    real(dp), intent(inout)            :: crch_stor
    real(dp), intent(in)               :: deni_rate
    real(dp), intent(in)               :: length
    real(dp), intent(inout)            :: q_yearly

    !Local variable
    real(dp)                :: f_temp
    real(dp)                :: N_stor
    real(dp)                :: f_conc
    real(dp)                :: width
    real(dp)                :: deni_amount
    real(dp)                :: rch_area

    !Nitrate stored in reach (m3 * g/m3 = g)
    N_stor = rch_stor * crch_stor

    !temperature factor from Yang et al., 2018 WRR
    f_temp = 2.0_dp ** ((stream_temp - 20.0_dp) / 10.0_dp)
    if (stream_temp < 5.0_dp) f_temp = f_temp * (stream_temp / 5.0_dp)
    if (stream_temp < 0.0_dp) f_temp = 0.0_dp

    !concentration factor (Yang et al., 2018)
    f_conc = crch_stor/(crch_stor + 1.5_dp)

    !calculate beneath area of a reach (rch_area) !from Yang et al., 2018 WRR
    q_yearly = q_yearly + (outflow - q_yearly) / (365.0_dp )  
    width = 5.40_dp * q_yearly ** (0.50_dp) 
    rch_area = width * length

    !instream denitrification (only applied for daily deni[1/day] dt = 1 day
    deni_amount = min(deni_rate * f_temp * f_conc * rch_area * 1000.0_dp, 0.5_dp * N_stor)

!print '(10f12.2)', deni_amount/N_stor, f_temp, f_conc
    !update N_stor due to denitrification
    N_stor = N_stor - deni_amount
    
    !update nitrate concentration in reach storage
    crch_stor = (N_stor + inflow * cinflow * 86400.0_dp)/(rch_stor + inflow * 86400.0_dp)

    !calulate outflow concentration
    coutflow = crch_stor

  end subroutine instream_deni

  !************************************************************************************************
  !Instream water routing: Muskingum
  !************************************************************************************************  
  subroutine muskingum(pre_inflow,     & 
                       inflow,         &
                       pre_outflow,    &
                       outflow,        &
                       x,              &
                       musk_coef_1,    &
                       musk_coef_2,    &
                       length,         &
                       width,          &
                       depth,          &
                       bank_slope,     &
                       chanel_slope,   &
                       maning_n,       &
                       rch_stor)

    implicit none

    !Input
    real(dp), intent(inout)        :: pre_inflow    ! [m3/s]    
    real(dp), intent(in)           :: inflow        ! [m3/s]
    real(dp), intent(inout)        :: pre_outflow   ! [m3/s]
    real(dp), intent(out)          :: outflow       ! [m3/s]
    real(dp), intent(in)           :: x             ! [-]
    real(dp), intent(in)           :: musk_coef_1   ! [-]
    real(dp), intent(in)           :: musk_coef_2   ! [-]   
    real(dp), intent(in)           :: length        ! [m]
    real(dp), intent(in)           :: width         ! [m]
    real(dp), intent(in)           :: depth         ! [m]
    real(dp), intent(in)           :: bank_slope    ! [-]
    real(dp), intent(in)           :: chanel_slope
    real(dp), intent(in)           :: maning_n
    real(dp), intent(inout)        :: rch_stor      !m3

    !Local variable
    integer(i4) :: i 
    integer(i4) :: n
    real(dp)    :: k_bnkfull 
    real(dp)    :: k01_bnkfull
    real(dp)    :: k
    real(dp)    :: max_dt
    real(dp)    :: min_dt
    real(dp)    :: dt
    real(dp)    :: c1
    real(dp)    :: c2
    real(dp)    :: c3
    real(dp)    :: vol         !m3
    real(dp)    :: q_out       !m3/s

    !Calculate k_bnkfull and k01_bnkfull
    k_bnkfull = kbankfull(depth, width, length, bank_slope, chanel_slope, maning_n)                                                                      ! [s]
    k01_bnkfull = kbankfull(0.1_dp * depth, width, length, bank_slope, chanel_slope, maning_n)  

    !Estimate K (stor_const) value
    k = (musk_coef_1 * k_bnkfull +  musk_coef_2 * k01_bnkfull)/(musk_coef_1 + musk_coef_2)

    !calculate max and min allowable time step
    min_dt = 2.0_dp * k * x
    max_dt = 2.0_dp * k * (1.0_dp - x)

    ! discretize time interval to meet the stability criterion
    n = find_n(min_dt, max_dt)

    if (n .gt. 0_i4) then

      !number of iteration
      dt = 86400_i4/n

      c1 = (dt - 2.0_dp * k * x)/ (2.0_dp * k * (1.0_dp - x) + dt)
      c2 = (dt + 2.0_dp * k * x)/ (2.0_dp * k * (1.0_dp - x) + dt)
      c3 = (2.0_dp * k * (1.0_dp - x) - dt)/ (2.0_dp * k * (1.0_dp - x) + dt)

      outflow = (c1 * inflow + c2 * pre_inflow + c3 * pre_outflow)
      vol = (inflow - outflow)* 86400.0_dp + rch_stor

      !check if qout > qin + vol
      if (outflow .ge. (rch_stor/86400.0_dp + inflow)) then 
        outflow = rch_stor/86400.0_dp + inflow
        vol = 0.0
      end if

      if (vol .le. 0.0_dp) then
        vol = 0.0_dp
        outflow = rch_stor/86400.0_dp + inflow
      end if 
     
    else
      outflow = inflow
      vol = rch_stor
    end if

    !update reach storage/previous states
    rch_stor = vol
    pre_inflow = inflow
    pre_outflow = outflow

  end subroutine muskingum
  
  !************************************************************************************************
  !Bankfull K calculation
  !************************************************************************************************
  function kbankfull(depth, width, length, bank_slope, chanel_slope, maning_n)
 
    implicit none
    real(dp), intent(in)   :: depth
    real(dp), intent(in)   :: width
    real(dp), intent(in)   :: length
    real(dp), intent(in)   :: bank_slope
    real(dp), intent(in)   :: chanel_slope
    real(dp), intent(in)   :: maning_n

    !local variable
    real(dp)               :: kbankfull
    real(dp)               :: wet_area
    real(dp)               :: wet_perimeter
    real(dp)               :: hyd_radius
    real(dp)               :: celerity

    wet_area = width * depth + depth ** 2.0/ bank_slope
    wet_perimeter = width + 2.0_dp * depth * sqrt(1.0_dp/(bank_slope ** 2.0_dp) + 1)
    hyd_radius = wet_area/wet_perimeter
    celerity = (5.0_dp/3.0_dp) * hyd_radius ** (2.0_dp/3.0_dp) * sqrt(chanel_slope)/maning_n
    kbankfull = length/celerity

  end function kbankfull

  !Function to find an integer number n between two numbers that is 86400/n is also in integernumber
  function find_n(low_bound, up_bound)

    implicit none

    real(dp), intent(in) :: low_bound, up_bound
    integer(i4)          :: up_limit, low_limit
    integer(i4)          :: find_n
    real(dp)             :: temp
    
    up_limit = int(86400.0_dp/low_bound)
    low_limit = int(86400.0_dp/up_bound)

    if (low_limit .ge. up_limit) then 
      find_n = 0_i4
    else
      temp = (86400.0_dp/low_bound + 86400.0_dp/up_bound)/2.0_dp
      find_n = int(temp)
      if ((temp - int(temp)) .ge. 0.5_dp) find_n = find_n + 1_i4 
!      print*, "tam nnn", temp, int(temp), find_n
    end if

    ! Abort routing if too many iterations 
    if(find_n .gt. 10000_i4) find_n = 0_i4

  end function find_n

  !************************************************************************************************
  !Read routing data
  !************************************************************************************************
  subroutine read_routing_data(L11_routing)

   implicit none

   type(routing_para), intent(inout)  :: L11_routing

   open(10, file = trim(adjustl(dirMorpho(1)))//trim(adjustl('muskingum.txt'))) 
     read(10,*)
     read(10,*) L11_routing%length(:)
     read(10,*) L11_routing%width(:)
     read(10,*) L11_routing%depth(:)
     read(10,*) L11_routing%bank_slope(:)
     read(10,*) L11_routing%chanel_slope(:)
     read(10,*) L11_routing%maning_n(:)
   close(10)

   !convert km to m
   L11_routing%length(:) = L11_routing%length(:) * 1000.0_dp

  end subroutine read_routing_data

  !************************************************************************************************
  !Instream water temperature (reference Yang et al., 2018 WRR)
  !************************************************************************************************
  subroutine stream_temperature(stream_temp, &
                                cell_temp,   &
                                cell_area)

    implicit none

    real(dp), intent(inout), dimension(:)      :: stream_temp  !dim = 3: Haus = 1, Meis = 2, Silber = 3 
    real(dp), intent(in),    dimension(:)      :: cell_temp    !dim = 533
    real(dp), intent(in),    dimension(:,:)    :: cell_area    !dim(1:533, 1:3)

    real(dp)                :: temp
    integer(i4)             :: i, j

    do i = 1, 3
      temp = 0.0_dp
      do j = 1, 533 !533 cells
        temp = temp + (cell_area(j,i) * cell_temp(j)) 
      end do
      stream_temp(i) = stream_temp(i) + (temp/sum(cell_area(:,i)) - stream_temp(i))/20.0_dp
    end do

  end subroutine stream_temperature

end module mo_routing






