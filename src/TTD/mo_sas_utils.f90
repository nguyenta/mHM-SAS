!>       \file mo_sas_utils.f90

!>       \brief some ultilities functions for solving the storage selection function

!>       \details some ultilities functions for solving the storage selection function

!>       \authors Tam Nguyen

!>       \date June 2020

MODULE mo_sas_utils

  use mo_kind,                                only : i4, dp
  use mo_message,                             only : message 

  implicit none

  public :: cdfbeta

contains

  function cdfbeta (x, a, b)
  ! ------------------------------------------------------------------

  !    NAME
  !        cdfbeta

  !    PURPOSE
  !>       \brief calculate value of the cummulative beta distribution function at x

  !>       \details calculate value of the cummulative beta distribution function at x
  !>        cdfbeta(x,a,b)   = beta(x,a,b)/beta(a,b) = incomplete beta function / complete beta function

  !    INTENT(IN)
  !>       \param[in] "real(dp) :: x" input array
  !>       \param[in] "real(dp) :: a" parameter of the beta function
  !>       \param[in] "real(dp) :: b" parameter of the beta function

  !    RETURN
  !>       \return real(dp) :: value of the beta function at x

  !    HISTORY
  !>       \authors Majumder, K.L., Bhattacharjee, G.P. 

  !    REFERENCE
  !>       Majumder, K.L., Bhattacharjee, G.P. (1973).Algorithm AS 63: The incomplete Beta Integral, 
  !>       Applied Statistics, 22(3), 409-411

  !    MODIFIED
  !>       Tam Nguyen (June, 2020)

  implicit none

  real(dp), parameter :: acu = 0.1e-14_dp
  real(dp)            :: ai
  real(dp)            :: beta
  real(dp)            :: cdfbeta
  real(dp)            :: cx
  logical             :: indx
  integer(i4)         :: ns
  real(dp)            :: a
  real(dp)            :: pp
  real(dp)            :: psq
  real(dp)            :: b
  real(dp)            :: qq
  real(dp)            :: rx
  real(dp)            :: temp
  real(dp)            :: term
  real(dp)            :: x
  real(dp)            :: xx

  beta = log(gamma ( a ) ) + log(gamma ( b ) ) - log(gamma ( a + b ))  
  cdfbeta = x

  !Special cases.
  if ( x == 0.0_dp .or. x == 1.0_dp ) then
    return
  end if

  !Change tail if necessary and determine S.
  psq = a + b
  cx = 1.0_dp - x

  if ( a < psq * x ) then
    xx = cx
    cx = x
    pp = b
    qq = a
    indx = .true.
  else
    xx = x
    pp = a
    qq = b
    indx = .false.
  end if

  term = 1.0_dp
  ai = 1.0_dp
  cdfbeta = 1.0_dp
  ns = int ( qq + cx * psq )

  !Use Soper's reduction formula.
  rx = xx / cx
  temp = qq - ai
  if ( ns == 0_i4 ) then
    rx = xx
  end if

  do

    term = term * temp * rx / ( pp + ai )
    cdfbeta = cdfbeta + term
    temp = abs ( term )

    if ( temp <= acu .and. temp <= acu * cdfbeta ) then

      cdfbeta = cdfbeta * exp ( pp * log ( xx ) &
        + ( qq - 1.0_dp ) * log ( cx ) - beta ) / pp

      if ( indx ) then
        cdfbeta = 1.0_dp - cdfbeta
      end if

      exit

    end if

    ai = ai + 1.0_dp
    ns = ns - 1_i4

    if ( 0_i4 <= ns ) then
      temp = qq - ai
      if ( ns == 0_i4 ) then
        rx = xx
      end if
    else
      temp = psq
      psq = psq + 1.0_dp
    end if

  end do

  return

  end function cdfbeta
  ! ------------------------------------------------------------------

  !    NAME
  !        cumsum

  !    PURPOSE
  !>       \brief calculate cummulative sum of an array

  !>       \details calculate cummulative sum of an array
  !>       \cumsum[x]   = /x1, x1 + x2,..., x1 + ... + xn/

  !    INTENT(IN)
  !>       \param[in] "real(dp) :: x" input array

  !    RETURN
  !>       \return real(dp) :: array of cummulative summation of x

  !    HISTORY
  !>       \authors Tam Nguyen

  !>       \date June 2020

  function cumsum(x)
    implicit none
    
    real(dp), dimension(:), intent(in)       :: x
    real(dp), dimension(size(x))             :: cumsum
    integer(i4)                              :: i

    !initialize result
    cumsum = 0.0_dp

    !first element of the output array
    cumsum(1) = x(1)

    !calculate cumulative summation
    if(size(x) > 1) then
       do i = 2,size(x)
          cumsum(i)  = cumsum(i-1) + x(i)
       end do
    end if
   
  end function cumsum

  ! ------------------------------------------------------------------

  !    NAME
  !        eval_sas

  !    PURPOSE
  !>       \brief caculate value of the sas function (beta or powerlaw)

  !>       \details calculate the powerlaw or beta distribution with given the funciton parameters
  !>       \eval_sas[x,ka]   = powerlaw(x,ka) = x ** ka
  !>       \eval_sas[x,ka,b] = beta(x,ka,b) = see the cdfbeta function

  !    INTENT(IN)
  !>       \param[in] "real(dp) :: x" input array

  !    RETURN
  !>       \return real(dp) :: values of the powerlaw or beta at x

  !    HISTORY
  !>       \authors Tam Nguyen

  !>       \date June 2020

  function eval_sas(x, sas_function, ka, b)
    implicit none

    real(dp),     dimension(:),     intent(in)    :: x   
    integer(i4),                    intent(in)    :: sas_function  
    real(dp),                       intent(in)    :: ka      !parameter k (or a) of the powerlaw (or beta) function
    real(dp),                       intent(in)    :: b       !parameter b of the beta function

    !local variables
    real(dp), dimension(size(x))                  :: eval_sas
    integer(i4)                                   :: i, ifault

    !check type of the SAS function
    if (sas_function == 1) then        !powerlaw
       goto 10
    else if (sas_function == 2) then   !beta function
       go to 20
    else                               !unknown funciton
       call message('  Unknown selection function for TTDs  ')
       stop
    end if

    !cummulative powerlaw distribution function
    !if the SAS function is the powerlaw, then ka is the k parameter
10  continue
    eval_sas = x(:) ** ka  !cummulative SAS function
    go to 30

    !cummulative beta distribution function
    !if SAS function is the beta function, ka and b are a, b
20  continue
    do i = 1, size(x)
       eval_sas(i) = cdfbeta(x(i), ka, b)
    end do 

30  continue 

  end function eval_sas

END MODULE mo_sas_utils






