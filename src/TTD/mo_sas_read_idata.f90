!>       \file mo_sas_read_idata.f90

!>       \brief read input data

!>       \details read input data from ASCII from text file

!>       \authors Tam Nguyen

!>       \date June 2020

module mo_sas_read_idata

  
  use mo_kind,                   only: i4, dp
  use mo_sas_global_variables,   only: sas_function,        &
                                       max_age,             &
                                       max_old_fraction,    &
                                       dir_input_sas,       &
                                       dir_output_sas,      &
                                       file_sas,            &
                                       L1_sas,              &
                                       start_warm_up,       &
                                       end_warm_up,         &
                                       iter_warm_up                     
  implicit none

  public :: read_sas_config

contains

  ! ------------------------------------------------------------------

  !    NAME
  !        read_sas_config

  !    PURPOSE
  !>       \brief read sas configuration and input data 

  !>       \details read sas input data
  !>       <txt> sas_config.txt      configuration for the sas function
  !>       <txt> initial_rtd.txt     initial residence time distribution

  !    INTENT(IN)
  !>       <txt> sas_config.txt

  !    INTENT(IN), OPTIONAL
  !>       
  !>       
  !    HISTORY
  !>       \authors Tam Nguyen

  !>       \date June 2020

  subroutine read_sas_config

    use mo_nml,              only: open_nml, close_nml, position_nml

    implicit none

    !local variables
    integer(i4)         :: i, j, funit, itmax !counter for loop

    namelist /sas_config/ dir_input_sas,     &
                          dir_output_sas,    &
                          itmax,             &
                          sas_function,      &
                          max_age,           &
                          max_old_fraction,  &
                          start_warm_up,     &
                          end_warm_up,       &
                          iter_warm_up

    funit = 999

    call open_nml(file_sas, funit, quiet=.true.)

    call position_nml('sas_config', funit)

    read(funit, nml = sas_config)

    call close_nml(funit)

    !Allocate intitial residence time distribution and concentration distribution
    allocate(L1_sas(1)%stor_age(itmax))
    allocate(L1_sas(1)%conc_age(itmax))
    allocate(L1_sas(2)%stor_age(itmax))
    allocate(L1_sas(2)%conc_age(itmax))
    allocate(L1_sas(3)%stor_age(itmax))
    allocate(L1_sas(3)%conc_age(itmax))

    !Read the initial residence time distribution & concentration time distribution
    open(funit, file = trim(adjustl(dir_input_sas)) // trim("ini_storage_conc.txt"))

      !skip header 
      read(funit,*)
 
      !read residence time distribution from "ini_storage_conc.txt"
      do i = 1, itmax
        read(funit, *) L1_sas(1)%stor_age(i), L1_sas(1)%conc_age(i) 
      end do
      L1_sas(2)%stor_age(:) = L1_sas(1)%stor_age(:)
      L1_sas(2)%conc_age(:) = L1_sas(1)%stor_age(:)
      L1_sas(3)%stor_age(:) = L1_sas(1)%stor_age(:)
      L1_sas(3)%conc_age(:) = L1_sas(1)%stor_age(:)      

    close(funit)

  end subroutine read_sas_config

end module mo_sas_read_idata